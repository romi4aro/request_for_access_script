import httplib2
import os
import sys
import json
import time

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from googleapiclient.errors import HttpError
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from sys import platform

SCOPES = 'https://www.googleapis.com/auth/drive'        # link on access rights

CLIENT_SECRET_FILE = 'client_secret.json'               # oauth2 token file

CRED_STORAGE = 'credit_storage.json'                    # keep refresh token

P_ERROR_LOG = 'erlog.txt'

OLD_USER_MAIL = input('Enter your old account email: ')

PAGESIZE = 99                                           # size of item array in api answer

APPLICATION_NAME = 'request_for_access'

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser])
    flags.add_argument('-f', '--folder', action='store_true', help='Copy only folders')
    flags = flags.parse_args()
except ImportError:
    flags = None


def get_credentials():

    file_path = os.path.dirname(os.path.abspath(__file__))
    credential_path = os.path.join(file_path, 'credit_storage.json')
    store = Storage(credential_path)
    credentials = store.get()                                               # get array from our json
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)   # take my secret file
        flow.user_agent = APPLICATION_NAME                                  # user_agent name in header

        if flags:
            credentials = tools.run_flow(flow, store, flags)                # send our response with client_secret
        else:  # Needed only for compatibility with Python 2.6              # and get refresh token
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def main():
    open(P_ERROR_LOG, "w")    
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)

    get_user_information = service.about().get(fields="user").execute()

    files = {}
    files = getlist(service, 'files')      

    if len(files['files']) != 0:
        brouser_request(files)
    else:
        print('SHARED FILES NOT FOUND!')   
    print('The End!')

    # Cleaning file with refresh_token
    file_path = os.path.dirname(os.path.abspath(__file__))
    credential_path = os.path.join(file_path, 'credit_storage.json')
    open(credential_path, "w")

    

        
def getlist(service, mimetype):

    pageiter = 0
    try:
        file_list = service.files().list(pageSize=PAGESIZE, q="trashed=false "        
                                         "and not '" + OLD_USER_MAIL + "' in owners "
                                                              "and sharedWithMe=true ",
                                         fields="nextPageToken, "
                                                "files(kind, id, name, mimeType, parents, owners, permissions)") \
            .execute()
    except Exception as e:
        print('!!!!!!!!!!!\n!!! EXCEPTION LOADLIST PROBLEM\n {0}\n!!!!!!!!!!!'.format(e).encode('utf-8'))
        credentials = get_credentials()
        http = credentials.authorize(httplib2.Http())
        service = discovery.build('drive', 'v3', http=http)
        file_list = service.files().list(pageSize=PAGESIZE, q="trashed=false "        
                                         "and not '" + OLD_USER_MAIL + "' in owners "
                                                              "and sharedWithMe=true ",
                                         fields="nextPageToken, "
                                                "files(kind, id, name, mimeType, parents, owners, permissions)") \
            .execute()
        print('\n LOADING FILELIST\n PAGE ' + str(pageiter))

    if 'nextPageToken' in file_list:
        next_page_token = file_list['nextPageToken']
        while next_page_token != 'False':
            pageiter += 1

            try:
                next_page = sservice.files().list(pageSize=PAGESIZE, q="trashed=false "        
                                         "and not '" + OLD_USER_MAIL + "' in owners "
                                                              "and sharedWithMe=true ",
                                         fields="nextPageToken, "
                                                "files(kind, id, name, mimeType, parents, owners, permissions)") \
            .execute()
            except Exception as e:
                print('!!!!!!!!!!!\n!!! EXCEPTION LOADLIST PROBLEM\n {0}\n!!!!!!!!!!!'.format(e).encode('utf-8'))
                credentials = get_credentials()
                http = credentials.authorize(httplib2.Http())
                service = discovery.build('drive', 'v3', http=http)
                next_page = sservice.files().list(pageSize=PAGESIZE, q="trashed=false "        
                                         "and not '" + OLD_USER_MAIL + "' in owners "
                                                              "and sharedWithMe=true ",
                                         fields="nextPageToken, "
                                                "files(kind, id, name, mimeType, parents, owners, permissions)") \
            .execute()

            for item in next_page['files']:
                file_list['files'].append(item)
            if not 'nextPageToken' in next_page:
                next_page_token = 'False'
            else:
                next_page_token = next_page['nextPageToken']

            print('\nPAGE' + mimetype + ' ' + str(pageiter))

    return file_list


def brouser_request(files):
    try:
        driver = webdriver.Firefox(executable_path="geckodriver.exe")
        i = True
        for file in files['files']:        
         if i:
             try:
                url = r"https://docs.google.com/file/d/"+file['id']
                driver.get(url)
                time.sleep(120)
                body = driver.find_element_by_tag_name("body")
                body.send_keys(Keys.COMMAND + 't')
                i = False
             except:
                print("PROBLEM WITH BROUSER OR THE TIME TO SIGN IN TO YOUR ACCOUNT HAS EXPIRED")
         else:
            driver.find_element_by_tag_name('body').send_keys(Keys.COMMAND + 'w') 
            url = r"https://docs.google.com/file/d/"+file['id']
            driver.get(url)
            element = driver.find_element_by_id('requestButton')
            element.click()
            time.sleep(2)
            body = driver.find_element_by_tag_name("body")
            body.send_keys(Keys.COMMAND + 't')
        driver.quit()
    except:
        print("PROBLEM WITH DRIVER FILE! PLEASE CHECK THE BROUSER DRIVER FILE!")



if __name__ == '__main__':
    main()
